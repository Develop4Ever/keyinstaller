#!/usr/bin/env python
# -*- coding: utf-8 -*-

import apt, os

DirConf = 'Config_File/'
DirDns = 'Config_File/Dns/'
Wvdial_conf = '/etc/wvdial.conf'
Dns_conf = '/etc/resolv.conf'
class installer():

    def Install(self, KeySelect):

        self.Cache = apt.Cache()
        self.WVDial = self.Cache["wvdial"]
        if self.WVDial.is_installed:
            pass
        else:
            os.system("apt-get install wvdial")

        if KeySelect == "Tre":

            self.ModifyConf = open(Wvdial_conf, 'w')
            self.Conf = open(DirConf+'Tre.txt', 'r')

            for self.Word in self.Conf:
                self.ModifyConf.write(self.Word)

            self.ModifyConf.close()

            self.ModifyDns = open(Dns_conf, 'w')
            self.Dns = open(DirDns+'Tre.txt', 'r')

            for self.Word in self.Dns:
                self.ModifyDns.write(self.Word)

            self.ModifyDns.close()

        if KeySelect == "Vodafone":

            self.ModifyConf = open(Wvdial_conf, 'w')
            self.Conf = open(DirConf+'Vodafone.txt', 'r')

            for self.Word in self.Conf:
                self.ModifyConf.write(self.Word)

            self.ModifyConf.close()

            self.ModifyDns = open(Dns_conf, 'w')
            self.Dns = open(DirDns+'Vodafone.txt', 'r')

            for self.Word in self.Dns:
                self.ModifyDns.write(self.Word)

            self.ModifyDns.close()

        if KeySelect == "Tim":

            self.ModifyConf = open(Wvdial_conf, 'w')
            self.Conf = open(DirConf+'Tim.txt', 'r')

            for self.Word in self.Conf:
                self.ModifyConf.write(self.Word)

            self.ModifyConf.close()

            self.ModifyDns = open(Dns_conf, 'w')
            self.Dns = open(DirDns+'Tim.txt', 'r')

            for self.Word in self.Dns:
                self.ModifyDns.write(self.Word)

            self.ModifyDns.close()

        if KeySelect == "Wind":

            self.ModifyConf = open(Wvdial_conf, 'w')
            self.Conf = open(DirConf+'Wind.txt', 'r')

            for self.Word in self.Conf:
                self.ModifyConf.write(self.Word)

            self.ModifyConf.close()

            self.ModifyDns = open(Dns_conf, 'w')
            self.Dns = open(DirDns+'Wind.txt', 'r')

            for self.Word in self.Dns:
                self.ModifyDns.write(self.Word)

            self.ModifyDns.close()

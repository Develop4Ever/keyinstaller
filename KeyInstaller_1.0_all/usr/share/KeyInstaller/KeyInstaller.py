#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gtk, pygtk
import os, apt
from Installer import installer

class KeyInstaller():

    def __init__(self):

        self.ListStore_1 = gtk.ListStore(str)
        self.ComboBoxEntry_1 = gtk.ComboBoxEntry(self.ListStore_1, 0)
        self.Button_enter = gtk.Button('Installa')
        self.Button_enter.connect("clicked", self.InstallKey)
        self.VBox_1 = gtk.VBox()
        self.VBox_1.pack_start(self.ComboBoxEntry_1)
        self.VBox_1.pack_start(self.Button_enter)
        self.Window = gtk.Window()
        self.Window.set_border_width(5)
        self.Window.set_resizable(gtk.FALSE)
        self.Window.add(self.VBox_1)
        self.Window.set_focus(None)
        self.Window.show_all()

    def InstallKey(self, ComboBoxEntry_1):

        self.KeySelect = str(self.ComboBoxEntry_1.get_active_text())
        try:
            installer().Install(self.KeySelect)
            self.OK = gtk.MessageDialog(None,0,gtk.MESSAGE_INFO,gtk.BUTTONS_NONE,"Configurazione completata")
            self.OK.run()
            self.OK.destroy()
        except:
            self.NO = gtk.MessageDialog(None, 0, gtk.MESSAGE_WARNING, gtk.BUTTONS_NONE,"Operazione non riuscita")
            self.NO.run()
            self.NO.destroy()

    def main(self):

        self.ListKey = ['Tim', 'Wind', 'Vodafone', 'Tre']
        for self.KeyName in self.ListKey:
            self.ListStore_1.append([self.KeyName])
        gtk.main()




if __name__ == '__main__':

    KeyInstaller = KeyInstaller()
    KeyInstaller.main()
